---
title: "中文字符串排序"
date: 2018-04-16T13:38:55+08:00
draft: false
tags: ["js"]
---

偶尔在开发中会遇到中文字符串排序的问题。乍一想，这个功能肯定需要服务端支持。
但是仔细考虑下，常用汉字就3500多个，在要求不是非常严谨的情况下，忽略多音字
的问题，在client端可以做一个简版。

实现参考代码：[pinyin.js](https://gitlab.com/xianxiaow/xianxiaow.gitlab.io/snippets/1710282)

拼音字典文件 [pinyin-dics.js](/code/pinyin-dict.js)

更多详细内容，参考 https://github.com/hotoo/pinyin
